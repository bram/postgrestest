# postgrestest

Demonstrates SchemaSpy in a gitlab pipeline. 
Uses [Pagila](https://github.com/devrimgunduz/pagila) as a sample database.

Results are served by gitlab pages: https://bram.pages.science.ru.nl/postgrestest

## Usage
Clone this project or use at least the `.gitlab-ci.yml` file in your own project.
Make sure to import your database in the `script`-section of the yml file.

Set some CD variables:

```
POSTGRES_DB
POSTGRES_PASSWORD
POSTGRES_USER
```

Like in this screenshot:
![ci-cd-variables](images/ci-cd-variables.png)
